package com.msi.dev.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.msi.dev.model.Pessoa;

@Repository
@Transactional
public interface PessoaRepository extends CrudRepository<Pessoa, Long> {
	
	@Query("Select p from Pessoa p where p.nome like %?1%")
	List<Pessoa> findPessoByName(String nome);
	
	@Query("Select p from Pessoa p where p.cpf > 0")
	List<Pessoa> findcpf(long cpf);
}
