package com.msi.dev.TesteJava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication
@EntityScan (basePackages = "com.msi.dev.model")
@ComponentScan(basePackages = {"com.*"})
@EnableJpaRepositories  (basePackages = "com.msi.dev.repository" )
public class TesteJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TesteJavaApplication.class, args);
	}

}
