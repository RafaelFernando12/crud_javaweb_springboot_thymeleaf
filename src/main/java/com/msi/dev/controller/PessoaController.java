package com.msi.dev.controller;

import java.awt.print.Pageable;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.msi.dev.model.Pessoa;
import com.msi.dev.repository.PessoaRepository;

@Controller
public class PessoaController {

	@Autowired
	private PessoaRepository pessoaRepository;
	
	@GetMapping("/cadastropessoa")
	public ModelAndView inicio() {
		
		ModelAndView view = new ModelAndView("cadastro/cadastropessoa");
		view.addObject("objpessoa", new Pessoa());
		return view;
		
	}	
	
	@PostMapping(value="**/salvarpessoa",
			 consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE
			)
	public ModelAndView salvar(Pessoa objpessoa, @RequestParam("cpf") long cpf) {
		 List<Pessoa> pessoacpf = pessoaRepository.findcpf(cpf); /*Na primeira vez como a lista está vazia tem tamanho 0 e entra aqui para o primeiro cadastro*/
		 boolean jacadastrado = false;		
		 Iterable<Pessoa> pessoas = pessoaRepository.findAll();
		 if (pessoacpf.size() == 0) {
			 pessoaRepository.save(objpessoa);
			 	
				ModelAndView view = new ModelAndView("cadastro/cadastropessoa");
				
				view.addObject("pessoas", pessoas);
				view.addObject("objpessoa", new Pessoa());
				
				return view;
		 } else {																	/*Na segunda vez entra aqui, pois a lista n tem mais tamanho 0*/
			 																		/*busca o cpf no banco e compara, enquanto não encontra fica percorrendo*/
		 	for (int i = 0; i < pessoacpf.size() && jacadastrado == false; i++) {	/*quando é igual marca jacadastrado como true e entra no if que encerra o método voltando para tela de cadastro*/
		 		if(pessoacpf.get(i).getCpf() != objpessoa.getCpf()) {
		 			jacadastrado = false;
		 		}else {
		 			jacadastrado = true;
		 			if (jacadastrado == true) {
		 				ModelAndView view = new ModelAndView("cadastro/cadastropessoa");
		 				view.addObject("pessoas", pessoas);
		 				view.addObject("objpessoa", new Pessoa());
		 				return view;
		 			}
					/*
					 * System.out.println("cpf já cadastrado" + "cpf no banco: "+
					 * pessoacpf.get(i).getCpf() + " Cpf que quer cadastrar: " + pessoa.getCpf() );
					 */	 			
		 		}
		 	}
		 	pessoaRepository.save(objpessoa);										/*Após percorrer toda a lista e não encontra o cpf, cai aqui e realiza o cadastro*/
		 																	
		ModelAndView view = new ModelAndView("cadastro/cadastropessoa");
		Iterable<Pessoa> pessoa1 = pessoaRepository.findAll();
		view.addObject("pessoas", pessoa1);
		view.addObject("objpessoa", new Pessoa());
		
		return view;
		 }
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/listarpessoas")
	public ModelAndView listar() {
		ModelAndView view = new ModelAndView("cadastro/cadastropessoa");
		Iterable<Pessoa> pessoa = pessoaRepository.findAll();
		view.addObject("pessoas", pessoa);
		view.addObject("objpessoa", new Pessoa());
		return view;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/editarpessoa/{idpessoa}")
	public ModelAndView editar(@PathVariable("idpessoa") Long idpessoa) {
		Optional<Pessoa> pessoa = pessoaRepository.findById(idpessoa);
		
		ModelAndView view = new ModelAndView("cadastro/cadastropessoa");
		
		Iterable<Pessoa> pessoas = pessoaRepository.findAll();
		view.addObject("pessoas", pessoas);
		
		view.addObject("objpessoa", pessoa.get());
		return view;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/removerpessoa/{idpessoa}")
	public ModelAndView remover(@PathVariable("idpessoa") Long idpessoa) {
		
		
		pessoaRepository.deleteById(idpessoa); 
		
		ModelAndView view = new ModelAndView("cadastro/cadastropessoa");
		view.addObject("pessoas", pessoaRepository.findAll());
		view.addObject("objpessoa", new Pessoa());
		return view;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/pesquisarpessoa")
	public ModelAndView pesquisar(@RequestParam("nomepesquisa") String nompesquisa) {
		ModelAndView view = new ModelAndView("cadastro/cadastropessoa");
		view.addObject("pessoas", pessoaRepository.findPessoByName(nompesquisa));
		view.addObject("objpessoa", new Pessoa());
		return view;
	}
}
